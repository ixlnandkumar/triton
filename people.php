<!DOCTYPE html>
<!--
  Name: Piroll - Minimal & Clean Portfolio HTML Template
  Version: 1.0.0
  Author: robirurk, nK
  Website: https://nkdev.info
  Purchase: https://nkdev.info
  Support: https://nk.ticksy.com
  License: You must have a valid license purchased only from ThemeForest (the above link) in order to legally use the theme for your project.
  Copyright 2017.
-->

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Triton Communications Private Limited.</title>

    <meta name="description" content="Piroll - Clean & Minimal Portfolio HTML template.">
    <meta name="keywords" content="portfolio, clean, minimal, blog, template, portfolio website">
    <meta name="author" content="robirurk">

    <link rel="icon" type="image/png" href="assets/images/favicon.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500%7cNunito+Sans:400,600,700%7cPT+Serif:400,400i" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Stroke 7 -->
    <link rel="stylesheet" href="assets/bower_components/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="assets/bower_components/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/default-skin/default-skin.css">

    <!-- Piroll -->
    <link rel="stylesheet" href="assets/css/piroll.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>


</head>


<body>
    <?php include 'layout/header.php';?>


<div class="nk-main bg-white">

<!--        <div class="nk-gap-1"></div>-->

        <div class="container">

            <div class="nk-portfolio-list nk-isotope nk-isotope-3-cols">


                <div class="nk-isotope-item" data-filter="Design">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="ali-merchant.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/ali_m.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Ali Merchant</div>
                                <h2 class="portfolio-item-title h5 mb-0">Director</h2>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="munawar-syed.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/munawer_s.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Munawer Syed</div>
                                <h2 class="portfolio-item-title h5 mb-0">Director</h2>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="surojoy-banerjee.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/surojoy_b.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Surojoy Banerjee</div>
                                <h2 class="portfolio-item-title h5 mb-0">Advisory Consultant</h2>
                            </div>
                        </div>
                    </div>
                </div>

<!--                <div class="nk-isotope-item" data-filter="Print">-->
<!--                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">-->
<!--                        <a href="ashutosh.php" class="nk-portfolio-item-link"></a>-->
<!--                        <div class="nk-portfolio-item-image">-->
<!--                            <div style="background-image: url('assets/images/people/ashutosh.jpg')"></div>-->
<!--                        </div>-->
<!--                        <div class="nk-portfolio-item-info">-->
<!--                            <div>-->
<!--                                <div class="portfolio-item-category pb-10">Ashutosh Sawhney</div>-->
<!--                                <h2 class="portfolio-item-title h5 mb-0">CEO - Triton</h2>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->


                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="ullas-chopra.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/ulaas_c.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Ullas Chopra</div>
                                <h2 class="portfolio-item-title h5 mb-0">National Creative Director</h2>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="virendra-saini.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/virendra_s_thumb.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Virendra Saini</div>
                                <h2 class="portfolio-item-title h5 mb-0">Executive Director - Mumbai</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="jyotsna-parikh.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/jyotsna.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Jyotsna Parikh</div>
                                <h2 class="portfolio-item-title h5 mb-0">Creative Head - Mumbai</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="sam-husaini.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/sam.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Sam Husaini</div>
                                <h2 class="portfolio-item-title h5 mb-0">President - North</h2>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="naveen-saraswat.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/naveen_s.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Naveen Saraswat</div>
                                <h2 class="portfolio-item-title h5 mb-0">Executive Director - Delhi</h2>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="mayur-rana.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/mayur_r.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Mayur Rana</div>
                                <h2 class="portfolio-item-title h5 mb-0">Executive Director - Ahmedabad</h2>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="ganesh-kamath.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/ganesh_k.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Ganesh Kamath</div>
                                <h2 class="portfolio-item-title h5 mb-0">Executive Director - South</h2>
                            </div>
                        </div>
                    </div>
                </div>


<!--                <div class="nk-isotope-item" data-filter="Print">-->
<!--                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">-->
<!--                        <a href="rajesh-mehta.php" class="nk-portfolio-item-link"></a>-->
<!--                        <div class="nk-portfolio-item-image">-->
<!--                            <div style="background-image: url('assets/images/people/rajesh_m.jpg')"></div>-->
<!--                        </div>-->
<!--                        <div class="nk-portfolio-item-info">-->
<!--                            <div>-->
<!--                                <div class="portfolio-item-category pb-10">Rajesh Mehta</div>-->
<!--                                <h2 class="portfolio-item-title h5 mb-0">National Head Strategy Planning</h2>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

<!--                <div class="nk-isotope-item" data-filter="Print">-->
<!--                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">-->
<!--                        <a href="sainath-iyer.php" class="nk-portfolio-item-link"></a>-->
<!--                        <div class="nk-portfolio-item-image">-->
<!--                            <div style="background-image: url('assets/images/people/sainath_i.jpg')"></div>-->
<!--                        </div>-->
<!--                        <div class="nk-portfolio-item-info">-->
<!--                            <div>-->
<!--                                <div class="portfolio-item-category pb-10">Sainath Iyer</div>-->
<!--                                <h2 class="portfolio-item-title h5 mb-0">Media Director</h2>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="shripad_kulkarni.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/shripad_kulkarni.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Shripad Kulkarni</div>
                                <h2 class="portfolio-item-title h5 mb-0">President - Media</h2>
                            </div>
                        </div>
                    </div>
                </div>

<!--                <div class="nk-isotope-item" data-filter="Print">-->
<!--                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">-->
<!--                        <a href="adarsh-singh.php" class="nk-portfolio-item-link"></a>-->
<!--                        <div class="nk-portfolio-item-image">-->
<!--                            <div style="background-image: url('assets/images/people/aadarsh_s.jpg')"></div>-->
<!--                        </div>-->
<!--                        <div class="nk-portfolio-item-info">-->
<!--                            <div>-->
<!--                                <div class="portfolio-item-category pb-10">Adarsh Singh</div>-->
<!--                                <h2 class="portfolio-item-title h5 mb-0">Vice President - Media</h2>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="rohit-kaul.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/rohit_k.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Rohit Kaul</div>
                                <h2 class="portfolio-item-title h5 mb-0">CEO - DIGIMO</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Print">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3 nk-portfolio-item-sm">
                        <a href="preeti-sawarkar.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/images/people/preeti.jpg')"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div>
                                <div class="portfolio-item-category pb-10">Preeti Sawarkar</div>
                                <h2 class="portfolio-item-title h5 mb-0">CEO - ZtoA Marketing Solutions</h2>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="nk-gap-5"></div>
        </div>

        <!-- START: Pagination -->
        <!--<div class="nk-pagination bg-gray-1 nk-pagination-center">-->
            <!--<a href="#">Load More Works</a>-->
        <!--</div>-->
        <!-- END: Pagination -->



        <?php include 'layout/footer.php';?>

    </div>




    <!-- START: Scripts -->

    <!-- GSAP -->
    <script src="assets/bower_components/gsap/src/minified/TweenMax.min.js"></script>
    <script src="assets/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- nK Share -->
    <script src="assets/plugins/nk-share/nk-share.js"></script>

    <!-- Sticky Kit -->
    <script src="assets/bower_components/sticky-kit/dist/sticky-kit.min.js"></script>

    <!-- Jarallax -->
    <script src="assets/bower_components/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/bower_components/jarallax/dist/jarallax-video.min.js"></script>

    <!-- Flickity -->
    <script src="assets/bower_components/flickity/dist/flickity.pkgd.min.js"></script>

    <!-- Isotope -->
    <script src="assets/bower_components/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Photoswipe -->
    <script src="assets/bower_components/photoswipe/dist/photoswipe.min.js"></script>
    <script src="assets/bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script>

    <!-- Jquery Form -->
    <script src="assets/bower_components/jquery-form/dist/jquery.form.min.js"></script>

    <!-- Jquery Validation -->
    <script src="assets/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>

    <!-- Hammer.js -->
    <script src="assets/bower_components/hammer.js/hammer.min.js"></script>

    <!-- Social Likes -->
    <script src="assets/bower_components/social-likes/dist/social-likes.min.js"></script>

    <!-- NanoSroller -->
    <script src="assets/bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script>

    <!-- Keymaster -->
    <script src="assets/bower_components/keymaster/keymaster.js"></script>

    <!-- Piroll -->
    <script src="assets/js/piroll.min.js"></script>
    <script src="assets/js/piroll-init.js"></script>

    <!-- Demo -->
    <script src="assets/js/demo.js"></script>
    <!-- END: Scripts -->


</body>

</html>