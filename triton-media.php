<!DOCTYPE html>
<!--
  Name: Piroll - Minimal & Clean Portfolio HTML Template
  Version: 1.0.0
  Author: robirurk, nK
  Website: https://nkdev.info
  Purchase: https://nkdev.info
  Support: https://nk.ticksy.com
  License: You must have a valid license purchased only from ThemeForest (the above link) in order to legally use the theme for your project.
  Copyright 2017.
-->

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Triton Communications Private Limited.</title>

    <meta name="description" content="Piroll - Clean & Minimal Portfolio HTML template.">
    <meta name="keywords" content="portfolio, clean, minimal, blog, template, portfolio website">
    <meta name="author" content="robirurk">

    <link rel="icon" type="image/png" href="assets/images/favicon.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500%7cNunito+Sans:400,600,700%7cPT+Serif:400,400i" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Stroke 7 -->
    <link rel="stylesheet" href="assets/bower_components/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="assets/bower_components/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/default-skin/default-skin.css">

    <!-- Piroll -->
    <link rel="stylesheet" href="assets/css/piroll.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>


</head>


<body>
    <?php include 'layout/header.php';?>


<div class="nk-main bg-white">

<!--        <div class="nk-gap-1"></div>-->

        <!-- START: Digital. Modern. Creative -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--<div class="nk-gap-5 mnt-4"></div>-->

                    <h2 class="display-4">Triton MEDIA
<!--                    <span class="text-main">Branding</span>-->
                    </h2>
                    <!--<div class="nk-gap mnt-5"></div>-->
                    <p class="lead">Triton Media is the ‘knowledge-focus, media investment management division’ of Triton Communications Pvt. Ltd. It has consistently raised the bar on customer-satisfaction norms and invested its clientele with au courant market details, strategic data, technology facts and knowledge additions to outdo competing brands. </p>
                    <p class="lead">Triton Media is outfitted with the industry tools to size up plans, oversee activities, negotiate judiciously and decode consumer segmentation of brands; thereby providing clients’ year-long investments, unparalleled market insights, high efficiency processes and a very competitive buying power. </p>
                    <p class="lead">We are devoted to <b>‘path-breaking’</b> & <b>‘persuasive’</b> media solutions that help clients materialize their marketing goals and deliver result-oriented media programme, that augment awareness, improve bottom-line and further growth.</p>
                    <p class="lead">
                    <b>
                        <h5>Our Linchpins</h5>
                        <ul>
                            <li> “Understand client businesses, completely." </li>
                            <li> “If we have to do it, we do it with passion." </li>
                            <li> “One must do what is right." </li>
                            <li> “If we have fun doing it, it shows in the results." </li>
                            <li> “We will be creative and result-oriented as well." </li>
                        </ul>
                    </b>
                    </p>
                    <p class="lead">As the market turns volatile, Triton Media now readies for tomorrow, by investing in knowledge, technology and talent.</p>
                    <div class="nk-gap-2"></div>
                </div>
            </div>
        </div>
        <!-- END: Digital. Modern. Creative -->


    <!-- START: Partners -->
    <div class="bg-white">
        <div class="container clients">
            <h2 class="display-4">Clients</h2>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/first-flight.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/kalpataru.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/indigo.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/isdi.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                 <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/adani.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/mothers-recipe.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/force_motors_logo.svg.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/water_kingdom.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/gala.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/aquaguard.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/pidilite.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/guvera.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/rotomac.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/greaves-cotton.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/metro-shoes.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/honda.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/shapoorji_pallonji_group.png" alt="" class="nk-img-fit">
                    </div>
                </div>
                <div class="box">
                    <div class="nk-box-1">
                        <img src="assets/images/media-brands/yes-bank.png" alt="" class="nk-img-fit">
                    </div>
                </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="nk-gap-3"></div>
    <!-- END: Partners -->


        <div class="container">
            <h2 class="display-4">Case Studies</h2>
            <!-- START: Filter -->
<!--            <ul class="nk-isotope-filter nk-isotope-filter-active text-left">-->
<!--                <li class="active" data-filter="*">All</li>-->
<!--                <li data-filter="Classic">Classic</li>-->
<!--                <li data-filter="Recent">Recent</li>-->
<!--                <li data-filter="Tvc">TVC</li>-->
<!--            </ul>-->
            <!-- END: Filter -->

            <div class="nk-portfolio-list nk-isotope nk-isotope-2-cols">

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="media-case-study-1.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/media-case-study/1/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Aquaguard</div>
                                <h2 class="portfolio-item-title h5 mb-0">Paani Ka Doctor</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="media-case-study-2.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/media-case-study/2/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Aquaguard</div>
                                <h2 class="portfolio-item-title h5 mb-0">Paani Ka Doctor</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="media-case-study-3.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/media-case-study/3/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Aquaguard</div>
                                <h2 class="portfolio-item-title h5 mb-0">Paani Ka Doctor</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="media-case-study-4.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/media-case-study/4/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Aquaguard</div>
                                <h2 class="portfolio-item-title h5 mb-0">Paani Ka Doctor</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="media-case-study-5.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/media-case-study/5/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Aquaguard</div>
                                <h2 class="portfolio-item-title h5 mb-0">Paani Ka Doctor</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="media-case-study-6.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/media-case-study/6/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Aquaguard</div>
                                <h2 class="portfolio-item-title h5 mb-0">Jaldaan</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="media-case-study-7.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/media-case-study/7/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Fortune</div>
                                <h2 class="portfolio-item-title h5 mb-0">GHAR KA KHANA</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="media-case-study-8.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/media-case-study/8/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Fortune</div>
                                <h2 class="portfolio-item-title h5 mb-0">IIFA Utsavam</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="media-case-study-9.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/media-case-study/9/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Fortune</div>
                                <h2 class="portfolio-item-title h5 mb-0">Daily Soap</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="nk-gap-5"></div>
        </div>

        <!-- START: Pagination -->
<!--        <div class="nk-pagination bg-gray-1 nk-pagination-center">-->
<!--            <a href="#">Load More Works</a>-->
<!--        </div>-->
        <!-- END: Pagination -->


    <!-- START: Contact Info -->
    <div class="container">
        <h2 class="display-4">Contact us
            <!--                    <span class="text-main">Branding</span>-->
        </h2>
        <div class="nk-gap-2"></div>

        <div class="col-lg-6">
            <!-- START: Tabs -->
            <div class="nk-tabs">
                <!--<ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab">Mumbai</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab">Ahmedabad</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-3" role="tab" data-toggle="tab">Bengaluru</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#tabs-1-4" role="tab" data-toggle="tab">Delhi</a>
                    </li>
                </ul>-->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                        <ul class="nk-contact-info">
<!--                            <li>-->
<!--                                <strong>Address:</strong> Moderna House, 88/C, Mezzanine Floor, Old Prabhadevi Marg, Prabhadevi, Mumbai, Maharashtra 400025</li>-->
<!--                            <li>-->
<!--                                <strong>Phone:</strong> +91 22 4093 0500</li>-->
                            <li>
                                <strong>Email:</strong> <a href="mailto:media@tritoncom.com">media@tritoncom.com</a></li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                        <ul class="nk-contact-info">
<!--                            <li>-->
<!--                                <strong>Address:</strong> B907/908, Premium House, Opp. Gandhigram Station, Off Ashram House, Ahmedabad 380 009</li>-->
<!--                            <li>-->
<!--                                <strong>Phone:</strong> 079 4022 6551/52, 079 2658 6551/52</li>-->
                            <li>
                                <strong>Email:</strong> media@tritoncom.com</li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-3">
                        <ul class="nk-contact-info">
<!--                            <li>-->
<!--                                <strong>Address:</strong> #32, 3rd Floor, 2nd Block, Near Anglo-Indian Colony, Austin Town,Bengaluru 580 047</li>-->
<!--                            <li>-->
<!--                                <strong>Phone:</strong> 080 4242 0900/918</li>-->
                            <li>
                                <strong>Email:</strong> media@tritoncom.com</li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tabs-1-4">
                        <ul class="nk-contact-info">
<!--                            <li>-->
<!--                                <strong>Address:</strong> Triton Communications Private Limited, Upper Ground Floor, Plot no. 15, Sector 44, Gurgaon 122003</li>-->
                            <!--<li>-->
                            <!--<strong>Phone:</strong> 080 4242 0900/918</li>-->
                            <li>
                                <strong>Email:</strong> media@tritoncom.com</li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END: Tabs -->
        </div>

        <div class="nk-gap-3"></div>
    </div>
    <!-- END: Contact Info -->



    <?php include 'layout/footer.php';?>


    </div>




    <!-- START: Scripts -->

    <!-- GSAP -->
    <script src="assets/bower_components/gsap/src/minified/TweenMax.min.js"></script>
    <script src="assets/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- nK Share -->
    <script src="assets/plugins/nk-share/nk-share.js"></script>

    <!-- Sticky Kit -->
    <script src="assets/bower_components/sticky-kit/dist/sticky-kit.min.js"></script>

    <!-- Jarallax -->
    <script src="assets/bower_components/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/bower_components/jarallax/dist/jarallax-video.min.js"></script>

    <!-- Flickity -->
    <script src="assets/bower_components/flickity/dist/flickity.pkgd.min.js"></script>

    <!-- Isotope -->
    <script src="assets/bower_components/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Photoswipe -->
    <script src="assets/bower_components/photoswipe/dist/photoswipe.min.js"></script>
    <script src="assets/bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script>

    <!-- Jquery Form -->
    <script src="assets/bower_components/jquery-form/dist/jquery.form.min.js"></script>

    <!-- Jquery Validation -->
    <script src="assets/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>

    <!-- Hammer.js -->
    <script src="assets/bower_components/hammer.js/hammer.min.js"></script>

    <!-- Social Likes -->
    <script src="assets/bower_components/social-likes/dist/social-likes.min.js"></script>

    <!-- NanoSroller -->
    <script src="assets/bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script>

    <!-- Keymaster -->
    <script src="assets/bower_components/keymaster/keymaster.js"></script>

    <!-- Piroll -->
    <script src="assets/js/piroll.min.js"></script>
    <script src="assets/js/piroll-init.js"></script>

    <!-- Demo -->
    <script src="assets/js/demo.js"></script>
    <!-- END: Scripts -->


</body>

</html>