<!DOCTYPE html>
<!--
  Name: Piroll - Minimal & Clean Portfolio HTML Template
  Version: 1.0.0
  Author: robirurk, nK
  Website: https://nkdev.info
  Purchase: https://nkdev.info
  Support: https://nk.ticksy.com
  License: You must have a valid license purchased only from ThemeForest (the above link) in order to legally use the theme for your project.
  Copyright 2017.
-->

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Triton Communications Private Limited.</title>

    <meta name="description" content="Piroll - Clean & Minimal Portfolio HTML template.">
    <meta name="keywords" content="portfolio, clean, minimal, blog, template, portfolio website">
    <meta name="author" content="robirurk">

    <link rel="icon" type="image/png" href="assets/images/favicon.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500%7cNunito+Sans:400,600,700%7cPT+Serif:400,400i" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Stroke 7 -->
    <link rel="stylesheet" href="assets/bower_components/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="assets/bower_components/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/default-skin/default-skin.css">

    <!-- Piroll -->
    <link rel="stylesheet" href="assets/css/piroll.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>


</head>


<body>
<?php include 'layout/header.php';?>


<div class="nk-main">

        <!--<div class="nk-gap-6 mnt-4"></div>-->

        <div class="container-fluid">
            <div class="nk-portfolio-single nk-portfolio-single-half">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="nk-sidebar-sticky" data-offset-top="0">
                            <div class="nk-portfolio-info mlauto pl-40">
<!--                                <h1 class="nk-portfolio-title">AQUAGUARD – EUREKA FORBES LIMITED</h1>-->
                                <h1 class="nk-portfolio-title">JALDAAN- A social initiative by AQUAGUARD</h1>
                                <h4 class="nk-portfolio-title"></h4>
                                <div class="nk-portfolio-text">
                                    <p>Water-borne diseases claim millions of casualties in India every year because a majority of the population cannot afford water purifiers. Eureka Forbes mobilized mass participation by coaxing the <b>‘haves’</b> to share clean, healthy water with the <b>‘have-nots.’</b> This kicked off <b><i>‘Jaldaan’:</i> The ‘gift of water’ movement.</b> The campaign was driven through digital. The disruptive thought of <b>‘5 litre sehatmand paani pilao, tandurust bharat banao’</b> resonated in the video of a child engaging in an act of good.  It became a viral sensation, overnight.</p>
                                    <p>The initiative is making a real difference to several thousand lives, every day.  A classic case of how a small act can bring about a big change. Eureka Forbes donated Rs. 10, along with generous contributions from people from all quarters. To date, 3231258 have pledged to <b>Jaldaan</b> and 79 community and school water purification projects have been executed from the contributions received. Eureka Forbes and Triton struck gold at PMAA Dragons Asia and DMA Asia Awards for this tour de force. The juggernaut rolls on. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 bg-black">
                        <div class="nk-portfolio-images">
                            <div class="js-video">
                                <video poster="assets/case-study/8/poster.jpg" controls>
                                    <source src="assets/case-study/8/1.mp4" type="video/mp4">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>

                            <img src="assets/case-study/8/1.jpg" alt="case-study">
                            <img src="assets/case-study/8/2.jpg" alt="case-study">
                            <img src="assets/case-study/8/3.jpg" alt="case-study">
                            <img src="assets/case-study/8/4.jpg" alt="case-study">
                            <img src="assets/case-study/8/5.jpg" alt="case-study">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- START: Pagination -->
        <div class="nk-pagination nk-pagination-center">
            <div class="container">
                <a class="nk-pagination-prev" style="text-align: right;" href="case-study-7.php">
                    <span class="pe-7s-angle-left"></span> Previous Project</a>
                <a class="nk-pagination-center" href="work.php">
                    <span class="nk-icon-squares"></span>
                </a>
                <a class="nk-pagination-next" href="case-study-9.php">Next Project <span class="pe-7s-angle-right"></span> </a>
            </div>
        </div>
        <!-- END: Pagination -->



    <?php include 'layout/footer.php';?>

    </div>




    <!-- START: Scripts -->

    <!-- GSAP -->
    <script src="assets/bower_components/gsap/src/minified/TweenMax.min.js"></script>
    <script src="assets/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- nK Share -->
    <script src="assets/plugins/nk-share/nk-share.js"></script>

    <!-- Sticky Kit -->
    <script src="assets/bower_components/sticky-kit/dist/sticky-kit.min.js"></script>

    <!-- Jarallax -->
    <script src="assets/bower_components/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/bower_components/jarallax/dist/jarallax-video.min.js"></script>

    <!-- Flickity -->
    <script src="assets/bower_components/flickity/dist/flickity.pkgd.min.js"></script>

    <!-- Isotope -->
    <script src="assets/bower_components/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Photoswipe -->
    <script src="assets/bower_components/photoswipe/dist/photoswipe.min.js"></script>
    <script src="assets/bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script>

    <!-- Jquery Form -->
    <script src="assets/bower_components/jquery-form/dist/jquery.form.min.js"></script>

    <!-- Jquery Validation -->
    <script src="assets/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>

    <!-- Hammer.js -->
    <script src="assets/bower_components/hammer.js/hammer.min.js"></script>

    <!-- Social Likes -->
    <script src="assets/bower_components/social-likes/dist/social-likes.min.js"></script>

    <!-- NanoSroller -->
    <script src="assets/bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script>

    <!-- Keymaster -->
    <script src="assets/bower_components/keymaster/keymaster.js"></script>

    <!-- Piroll -->
    <script src="assets/js/piroll.min.js"></script>
    <script src="assets/js/piroll-init.js"></script>

    <!-- Demo -->
    <script src="assets/js/demo.js"></script>
    <!-- END: Scripts -->


</body>

</html>