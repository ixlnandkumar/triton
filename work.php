<!DOCTYPE html>
<!--
  Name: Piroll - Minimal & Clean Portfolio HTML Template
  Version: 1.0.0
  Author: robirurk, nK
  Website: https://nkdev.info
  Purchase: https://nkdev.info
  Support: https://nk.ticksy.com
  License: You must have a valid license purchased only from ThemeForest (the above link) in order to legally use the theme for your project.
  Copyright 2017.
-->

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Triton Communications Private Limited.</title>

    <meta name="description" content="Piroll - Clean & Minimal Portfolio HTML template.">
    <meta name="keywords" content="portfolio, clean, minimal, blog, template, portfolio website">
    <meta name="author" content="robirurk">

    <link rel="icon" type="image/png" href="assets/images/favicon.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500%7cNunito+Sans:400,600,700%7cPT+Serif:400,400i" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Stroke 7 -->
    <link rel="stylesheet" href="assets/bower_components/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="assets/bower_components/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/default-skin/default-skin.css">

    <!-- Piroll -->
    <link rel="stylesheet" href="assets/css/piroll.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>


</head>


<body>
    <?php include 'layout/header.php';?>


<div class="nk-main bg-white">

<!--        <div class="nk-gap-1"></div>-->

        <!-- START: Digital. Modern. Creative -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!--<div class="nk-gap-5 mnt-4"></div>-->

                    <!--<h2 class="display-4">We Create Digital-->
                    <!--<span class="text-main">Branding</span>-->
                    <!--</h2>-->
                    <!--<div class="nk-gap mnt-5"></div>-->
                    <p class="lead"> We have grown brands that started from the cradle as niche brands, and went on to own complete categories, viz. <b>Moov</b> and <b>Fortune Oils,</b> to name a few. Others have been built to challenge rules and have gone on to become symbols of hope and confidence, viz. <b>Yes Bank, Set Wet, Officer’s Choice</b> and many more. And then there are others that are born leaders and we ensure they stay crowd favourites. <b>Aquaguard</b> has been one of those long-distance champion runners. </p>
                    <div class="nk-gap"></div>
                </div>
            </div>
        </div>
        <!-- END: Digital. Modern. Creative -->

        <div class="container">
            <!-- START: Filter -->
            <ul class="nk-isotope-filter nk-isotope-filter-active text-left">
                <li class="active" data-filter="*">All</li>
                <li data-filter="Classic">Classic</li>
                <li data-filter="Recent">Recent</li>
<!--                <li data-filter="Tvc">TVC</li>-->
            </ul>
            <!-- END: Filter -->

            <div class="nk-portfolio-list nk-isotope nk-isotope-2-cols">


                <!--<div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-1.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/1/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">AQUAGUARD - EUREKA FORBES LIMITED</div>
                                <h2 class="portfolio-item-title h5 mb-0">Farak Dikhta Hai</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>-->

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-47.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/47/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Ambuja Cement</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-48.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/48/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">SAMCO Securities</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-2.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/2/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">FORTUNE RICE - ADANI WILMAR LIMITED</div>
                                <h2 class="portfolio-item-title h5 mb-0">KHANE KA MAZAA HAI KHILANE ME</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent" style="display: none">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-3.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/3/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Officer's Choice</div>
                                <h2 class="portfolio-item-title h5 mb-0">Salute toh banta hai</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-4.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/4/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">FORTUNE - ADANI WILMAR LIMITED</div>
                                <h2 class="portfolio-item-title h5 mb-0">Thoda aur chalega!</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-5.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/5/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Moov</div>
                                <h2 class="portfolio-item-title h5 mb-0">Aah se aaha tak</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-6.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/6/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">YES Bank</div>
                                <h2 class="portfolio-item-title h5 mb-0">India bole YES!</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-7.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/7/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">AQUAGUARD - EUREKA FORBES LIMITED</div>
                                <h2 class="portfolio-item-title h5 mb-0">SHUDH SE JYADA SEHAT KA VAADA</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-8.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/8/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
<!--                                <div class="portfolio-item-category pb-10">AQUAGUARD – EUREKA FORBES LIMITED</div>-->
                                <div class="portfolio-item-category pb-10">JALDAAN- A social initiative by AQUAGUARD</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-9.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/9/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">OFFICER’S CHOICE</div>
                                <h2 class="portfolio-item-title h5 mb-0">SALUTE TOH BANTA HAI</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-10.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/10/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">OFFICER’S CHOICE BLUE</div>
                                <h2 class="portfolio-item-title h5 mb-0">SALUTE TOH BANTA HAI</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-11.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/11/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Organic Harvest</div>
                                <h2 class="portfolio-item-title h5 mb-0">Only honesty</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-12.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/12/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Ralco Tyres</div>
                                <h2 class="portfolio-item-title h5 mb-0">Ride with trust</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

<!--                <div class="nk-isotope-item" data-filter="Tvc Recent">-->
<!--                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">-->
<!--                        <a href="work-13.php" class="nk-portfolio-item-link"></a>-->
<!--                        <div class="nk-portfolio-item-image">-->
<!--                            <div style="background-image: url('assets/case-study/13/thumb.jpg');"></div>-->
<!--                        </div>-->
<!--                        <div class="nk-portfolio-item-info">-->
<!--                            <div class="pull-left" style="display: table;width: auto!important;">-->
<!--                                <div class="portfolio-item-category pb-10">Ralco Tyres</div>-->
<!--                                <h2 class="portfolio-item-title h5 mb-0">Years of trust</h2>-->
<!--                            </div>-->
<!--                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-14.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/14/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Set Wet</div>
                                <h2 class="portfolio-item-title h5 mb-0">Very very sexy</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-15.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/15/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Wagh Bakri</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-16.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/16/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">FORTUNE SOYA BADI – ADANI WILMAR LIMITED</div>
                                <h2 class="portfolio-item-title h5 mb-0">Yum jo de champions ko dum</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-17.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/17/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Kingfisher buzz</div>
                                <h2 class="portfolio-item-title h5 mb-0">Life is delicious</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-18.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/18/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Ramdev Haldi</div>
                                <h2 class="portfolio-item-title h5 mb-0">Shudh Haldi</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-19.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/19/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">NATIONAL EGG CO-ORDINATION COMMITTEE</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

<!--                <div class="nk-isotope-item" data-filter="Recent">-->
<!--                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">-->
<!--                        <a href="work-20.php" class="nk-portfolio-item-link"></a>-->
<!--                        <div class="nk-portfolio-item-image">-->
<!--                            <div style="background-image: url('assets/case-study/20/thumb.jpg');"></div>-->
<!--                        </div>-->
<!--                        <div class="nk-portfolio-item-info">-->
<!--                            <div class="pull-left" style="display: table;width: auto!important;">-->
<!--                                <div class="portfolio-item-category pb-10">NECC</div>-->
<!--                                <h2 class="portfolio-item-title h5 mb-0"></h2>-->
<!--                            </div>-->
<!--                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-21.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/21/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Craftsvilla</div>
                                <h2 class="portfolio-item-title h5 mb-0">Desi by choice</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-22.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/22/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Fevicryl</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-23.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/23/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">AQUAGUARD – EUREKA FORBES LIMITED</div>
                                <h2 class="portfolio-item-title h5 mb-0">Yeh jal amrut hai</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-24.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/24/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Sandpiper</div>
                                <h2 class="portfolio-item-title h5 mb-0">AZAAD PANCHEE</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-25.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/25/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Shanti Oil</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent" style="display: none">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-26.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/26/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">FORTUNE – ADANI WILMAR LIMITED</div>
                                <h2 class="portfolio-item-title h5 mb-0">Paanch ka aashirwaad</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-27.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/27/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">MARCOPOLO STRONG BEER</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-28.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/28/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">ZINGARO STRONG BEER</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-29.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/29/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Vink Faucets</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-30.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/30/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Set wet zatak</div>
                                <h2 class="portfolio-item-title h5 mb-0">VERY VERY SEXY</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-31.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/31/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Set Wet Xtra Hold</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-32.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/32/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Prime tape</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-33.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/33/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Metro shoes</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-34.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/34/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Venky's</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-35.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/35/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">KINGS SOYABEAN OIL</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-36.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/36/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">LORD & MASTER PREMIUM BRANDY</div>
                                <h2 class="portfolio-item-title h5 mb-0">FOR THE MAN OF SUBSTANCE</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-37.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/37/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Kingfisher blue</div>
                                <h2 class="portfolio-item-title h5 mb-0">BLUE FREEDOM</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-38.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/38/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Ozone</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-39.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/39/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Dainik Bhaskar</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-40.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/40/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Jolly Roger</div>
                                <h2 class="portfolio-item-title h5 mb-0">GET HIGH ON LIFE…MAN!</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-41.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/41/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Malayala Manorama</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-42.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/42/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Reid & taylor</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

<!--                <div class="nk-isotope-item" data-filter="Classic">-->
<!--                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">-->
<!--                        <a href="work-43.php" class="nk-portfolio-item-link"></a>-->
<!--                        <div class="nk-portfolio-item-image">-->
<!--                            <div style="background-image: url('assets/case-study/43/thumb.jpg');"></div>-->
<!--                        </div>-->
<!--                        <div class="nk-portfolio-item-info">-->
<!--                            <div class="pull-left" style="display: table;width: auto!important;">-->
<!--                                <div class="portfolio-item-category pb-10">AG Dangerous Creatures</div>-->
<!--                                <h2 class="portfolio-item-title h5 mb-0"></h2>-->
<!--                            </div>-->
<!--                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="nk-isotope-item" data-filter="Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-44.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/44/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">BULLET SUPER STRONG BEER</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-45.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/45/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">S.KUMARS</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="work-46.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/46/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Rotomac</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="nk-gap-5"></div>
        </div>

        <!-- START: Pagination -->
<!--        <div class="nk-pagination bg-gray-1 nk-pagination-center">-->
<!--            <a href="#">Load More Works</a>-->
<!--        </div>-->
        <!-- END: Pagination -->



    <?php include 'layout/footer.php';?>


    </div>




    <!-- START: Scripts -->

    <!-- GSAP -->
    <script src="assets/bower_components/gsap/src/minified/TweenMax.min.js"></script>
    <script src="assets/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- nK Share -->
    <script src="assets/plugins/nk-share/nk-share.js"></script>

    <!-- Sticky Kit -->
    <script src="assets/bower_components/sticky-kit/dist/sticky-kit.min.js"></script>

    <!-- Jarallax -->
    <script src="assets/bower_components/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/bower_components/jarallax/dist/jarallax-video.min.js"></script>

    <!-- Flickity -->
    <script src="assets/bower_components/flickity/dist/flickity.pkgd.min.js"></script>

    <!-- Isotope -->
    <script src="assets/bower_components/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Photoswipe -->
    <script src="assets/bower_components/photoswipe/dist/photoswipe.min.js"></script>
    <script src="assets/bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script>

    <!-- Jquery Form -->
    <script src="assets/bower_components/jquery-form/dist/jquery.form.min.js"></script>

    <!-- Jquery Validation -->
    <script src="assets/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>

    <!-- Hammer.js -->
    <script src="assets/bower_components/hammer.js/hammer.min.js"></script>

    <!-- Social Likes -->
    <script src="assets/bower_components/social-likes/dist/social-likes.min.js"></script>

    <!-- NanoSroller -->
    <script src="assets/bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script>

    <!-- Keymaster -->
    <script src="assets/bower_components/keymaster/keymaster.js"></script>

    <!-- Piroll -->
    <script src="assets/js/piroll.min.js"></script>
    <script src="assets/js/piroll-init.js"></script>

    <!-- Demo -->
    <script src="assets/js/demo.js"></script>
    <!-- END: Scripts -->


</body>

</html>