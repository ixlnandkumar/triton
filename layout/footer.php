<?php echo '
        <!-- START: Footer -->
        <footer class="nk-footer">
            <div class="container">
                <div class="row vertical-gap">
                    <div class="col-lg-5">
                        <h3 class="h5 mb-20">Triton Communications Pvt. Ltd.</h3>
                        <p class="mb-0">&copy; 2017 Triton. All rights reserved.</p>
                    </div>
                    <div class="col-lg-3">
                        <!--<a href="mailto:virendra.saini@tritoncom.com">virendra.saini@tritoncom.com</a>-->
                        <br> +91 22 4093 0500
                    </div>
                    <div class="col-lg-2">
                        <div class="nk-links-list">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li><a href="philosophy.php">Philosophy</a></li>
                                <li><a href="work.php">Work</a></li>
                                <li><a href="people.php">People</a></li>
                                <li><a href="triton-media.php">Triton Media</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="nk-links-list">
                            <ul>
                                <li><a href="http://www.digimo.co.in" target="_blank">Digimo</a></li>
                                <!--<li><a href="http://www.metaphorexperiential.com" target="_blank">Metaphor</a></li>-->
                                <li><a href="career.php">Career</a></li>
                                <li><a href="contact-us.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END: Footer -->
    
'; ?>