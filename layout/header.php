<?php echo '

       <div class="social-div-m">
    <div class="social-div"><a href="https://www.facebook.com/TritonCommunications/" target="_blank"> <img src="assets/images/social/facebook-icon.png"/></a> </div>
    <!--<div class="social-div"><a href=""> <img src="assets/images/social/twitter-icon.png"/></a> </div>
    <div class="social-div"><a href=""> <img src="assets/images/social/linkedin-icon.png"/></a> </div>
    <div class="social-div"><a href=""> <img src="assets/images/social/instagram-icon.png"/></a> </div>-->
    <div class="social-div"><a href="https://www.youtube.com/channel/UCFIRtQdDEghGk00iMe5fOqw" target="_blank"> <img src="assets/images/social/youtube.png"/></a> </div>
</div>

<header class="nk-header nk-header-opaque">
    <!--
    START: Navbar

    Additional Classes:
        .nk-navbar-lg
        .nk-navbar-sticky
        .nk-navbar-autohide
        .nk-navbar-transparent
        .nk-navbar-transparent-always
        .nk-navbar-white-text-on-top
        .nk-navbar-dark
-->
    <nav class="nk-navbar nk-navbar-top nk-navbar-sticky nk-navbar-autohide">
        <div class="container">
            <div class="nk-nav-table">

                <a href="index.php" class="nk-nav-logo">

                    <img src="assets/images/triton-logo.png" alt="" width="200">
                </a>


                <ul class="nk-nav nk-nav-right nk-nav-icons">


                    <li class="single-icon">
                        <a href="#" class="nk-navbar-full-toggle">
                                <span class="nk-icon-burger">
                                    <span class="nk-t-1"></span>
                                    <span class="nk-t-2"></span>
                                    <span class="nk-t-3"></span>
                                </span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- END: Navbar -->

</header>


<!--
    START: Fullscreen Navbar

    Additional Classes:
        .nk-navbar-align-center
        .nk-navbar-align-right
-->
<nav class="nk-navbar nk-navbar-full nk-navbar-align-center" id="nk-full">
    <div class="nk-nav-table">
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-header">

                    <div class="nk-nav-logo">
                        <a href="index.php" class="nk-nav-logo">
                            <img src="assets/images/triton-logo-white.png" alt="" width="200">
                        </a>
                    </div>

                    <div class="nk-nav-close nk-navbar-full-toggle">
                        <span class="nk-icon-close"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row-full nk-nav-row">
            <div class="nano">
                <div class="nano-content">
                    <div class="nk-nav-table">
                        <div class="nk-nav-row nk-nav-row-full nk-nav-row-center">
                            <ul class="nk-nav">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="philosophy.php">Philosophy</a></li>
                                <li><a href="work.php">Work</a></li>
                                <li><a href="people.php">People</a></li>
                                <li><a href="triton-media.php">Triton Media</a></li>
                                <li><a href="http://www.digimo.co.in" target="_blank">Digimo</a></li>
                                <!--<li><a href="http://www.metaphorexperiential.com" target="_blank">Metaphor</a></li>-->
                                <li><a href="career.php">Career</a></li>
                                <li><a href="contact-us.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nk-nav-row">
            <div class="container">
                <div class="nk-nav-social">
                    <ul>
                        <li><a href="https://www.facebook.com/TritonCommunications/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <!--<li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                        <li><a href=""><i class="fa fa-instagram"></i></a></li>-->
                        <li><a href="https://www.youtube.com/channel/UCFIRtQdDEghGk00iMe5fOqw" target="_blank"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- END: Fullscreen Navbar -->
    
'; ?>