<!DOCTYPE html>
<!--
  Name: Piroll - Minimal & Clean Portfolio HTML Template
  Version: 1.0.0
  Author: robirurk, nK
  Website: https://nkdev.info
  Purchase: https://nkdev.info
  Support: https://nk.ticksy.com
  License: You must have a valid license purchased only from ThemeForest (the above link) in order to legally use the theme for your project.
  Copyright 2017.
-->

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Triton Communications Private Limited.</title>

    <meta name="description" content="Piroll - Clean & Minimal Portfolio HTML template.">
    <meta name="keywords" content="portfolio, clean, minimal, blog, template, portfolio website">
    <meta name="author" content="robirurk">

    <link rel="icon" type="image/png" href="assets/images/favicon.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500%7cNunito+Sans:400,600,700%7cPT+Serif:400,400i" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Stroke 7 -->
    <link rel="stylesheet" href="assets/bower_components/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="assets/bower_components/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/default-skin/default-skin.css">

    <!-- Piroll -->
    <link rel="stylesheet" href="assets/css/piroll.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>

    <style>
        .nk-carousel-3 .flickity-slider>div {
            height: 480px;
            width: 100%;
        }

        .nk-carousel-3 .flickity-slider>div>div {
            background-size: cover;
            background-position: 50% 50%;
        }
    </style>

</head>


<body>
    <?php include 'layout/header.php';?>


    <div class="nk-main bg-white">

<!--        <div class="nk-gap-1"></div>-->

        <div class="container">
        <!-- START: Carousel -->
        <div class="nk-carousel-3 nk-carousel-no-margin nk-carousel-all-visible nk-carousel-arrows text-white text-center" data-size="1" data-autoplay="18000" data-arrows="true">
            <div class="nk-carousel-inner">
                <div>
                    <div style="background-color: #7FD0D9">
                        <div class="nk-vertical-center">
                            <div>
                                <h1 class="nk-title text-white">THE GREATEST DISRUPTION <br>IS CHANGE.</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div style="background-color: #C4B290">
                        <div class="nk-vertical-center">
                            <div>
                                <h1 class="nk-title text-white">GREAT BRANDS ARE STORIES. <br>WELL TOLD.</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div style="background-color: #B19095">
                        <div class="nk-vertical-center">
                            <div>
                                <h1 class="nk-title text-white">PERSUASION IS NOT AN ACT. <br>IT’S AN ART.</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div style="background-color: #9B77A8">
                        <div class="nk-vertical-center">
                            <div>
                                <h1 class="nk-title text-white">HAPPY CLIENTS ARE  <br>THE BEST ADVERTISEMENTS.</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div style="background-color: #EDACB1">
                        <div class="nk-vertical-center">
                            <div>
                                <h1 class="nk-title text-white">DESIGN IS SILENCE THAT  <br>SPEAKS VOLUMES.</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div style="background-color: #8aa5d4;text-transform: uppercase;">
                        <div class="nk-vertical-center">
                            <div>
                                <h1 class="nk-title text-white">Everyday, something incredible <br>is waiting to say ‘hello'.</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Carousel -->
        </div>


        <!-- START: Digital. Modern. Creative -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="nk-gap-1"></div>

                    <!--<h2 class="display-4">We Create Digital-->
                    <!--<span class="text-main">Branding</span>-->
                    <!--</h2>-->
                    <div class="nk-gap mnt-5"></div>
                    <p class="lead">We are built around clients and consumers. We look for a higher degree of consolidation to make integration and interdependence, more effective. By re-bundling capabilities such as creative, strategy and media for individual needs, we bring something new to the table, but all the time, staying accountable to clients for results. Our intention is to create great work at the speed of the marketplace for cohesive and consistent brand experiences. So, what’s the one thing that drives Triton? Is it creative edge? Is it strategic consultation? Or is it media innovation? In fact, it’s a combine of all three-Results.</p>
                    <div class="nk-gap"></div>
                </div>
            </div>
        </div>
        <!-- END: Digital. Modern. Creative -->


        <div class="container">
            <!-- START: Filter -->
            <ul class="nk-isotope-filter nk-isotope-filter-active text-left">
                <li class="active" data-filter="*">Case studies</li>
<!--                <li class="active" data-filter="*">All</li>-->
<!--                <li data-filter="Classic">Classic</li>-->
<!--                <li data-filter="Recent">Recent</li>-->
<!--                <li data-filter="Tvc">TVC</li>-->
            </ul>
            <!-- END: Filter -->

            <div class="nk-portfolio-list nk-isotope nk-isotope-2-cols">

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="case-study-4.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/4/thumb2.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">FORTUNE - ADANI WILMAR LIMITED</div>
                                <h2 class="portfolio-item-title h5 mb-0">THODA AUR CHALEGA!</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="case-study-5.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/5/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Moov</div>
                                <h2 class="portfolio-item-title h5 mb-0">Aah se aaha tak</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="case-study-6.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/6/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">YES Bank</div>
                                <h2 class="portfolio-item-title h5 mb-0">India bole YES!</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="case-study-7.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/7/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">AQUAGUARD - EUREKA FORBES LIMITED</div>
                                <h2 class="portfolio-item-title h5 mb-0">SHUDH SE JYADA SEHAT KA VAADA</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="case-study-8.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/8/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
<!--                                <div class="portfolio-item-category pb-10">AQUAGUARD – EUREKA FORBES LIMITED</div>-->
                                <div class="portfolio-item-category pb-10">JALDAAN- A social initiative by AQUAGUARD</div>
                                <h2 class="portfolio-item-title h5 mb-0"></h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Classic">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="case-study-9.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/9/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Officer’s Choice</div>
                                <h2 class="portfolio-item-title h5 mb-0">SALUTE TOH BANTA HAI</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="nk-isotope-item" data-filter="Tvc Recent">
                    <div class="nk-portfolio-item nk-portfolio-item-info-style-3">
                        <a href="case-study-14.php" class="nk-portfolio-item-link"></a>
                        <div class="nk-portfolio-item-image">
                            <div style="background-image: url('assets/case-study/14/thumb.jpg');"></div>
                        </div>
                        <div class="nk-portfolio-item-info">
                            <div class="pull-left" style="display: table;width: auto!important;">
                                <div class="portfolio-item-category pb-10">Set Wet</div>
                                <h2 class="portfolio-item-title h5 mb-0">Very very sexy</h2>
                            </div>
                            <a class="nk-btn-2 pull-right pt-10" style="display: table">Read More</a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="nk-gap-5"></div>
        </div>

        <!-- START: Pagination -->
<!--        <div class="nk-pagination bg-gray-1 nk-pagination-center">-->
<!--            <a href="#">Load More Works</a>-->
<!--        </div>-->
        <!-- END: Pagination -->


        <?php include 'layout/footer.php';?>

    </div>




    <!-- START: Scripts -->

    <!-- GSAP -->
    <script src="assets/bower_components/gsap/src/minified/TweenMax.min.js"></script>
    <script src="assets/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- nK Share -->
    <script src="assets/plugins/nk-share/nk-share.js"></script>

    <!-- Sticky Kit -->
    <script src="assets/bower_components/sticky-kit/dist/sticky-kit.min.js"></script>

    <!-- Jarallax -->
    <script src="assets/bower_components/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/bower_components/jarallax/dist/jarallax-video.min.js"></script>

    <!-- Flickity -->
    <script src="assets/bower_components/flickity/dist/flickity.pkgd.min.js"></script>

    <!-- Isotope -->
    <script src="assets/bower_components/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Photoswipe -->
    <script src="assets/bower_components/photoswipe/dist/photoswipe.min.js"></script>
    <script src="assets/bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script>

    <!-- Jquery Form -->
    <script src="assets/bower_components/jquery-form/dist/jquery.form.min.js"></script>

    <!-- Jquery Validation -->
    <script src="assets/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>

    <!-- Hammer.js -->
    <script src="assets/bower_components/hammer.js/hammer.min.js"></script>

    <!-- Social Likes -->
    <script src="assets/bower_components/social-likes/dist/social-likes.min.js"></script>

    <!-- NanoSroller -->
    <script src="assets/bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script>

    <!-- Keymaster -->
    <script src="assets/bower_components/keymaster/keymaster.js"></script>

    <!-- Piroll -->
    <script src="assets/js/piroll.min.js"></script>
    <script src="assets/js/piroll-init.js"></script>

    <!-- Demo -->
    <script src="assets/js/demo.js"></script>
    <!-- END: Scripts -->


</body>

</html>