<!DOCTYPE html>
<!--
  Name: Piroll - Minimal & Clean Portfolio HTML Template
  Version: 1.0.0
  Author: robirurk, nK
  Website: https://nkdev.info
  Purchase: https://nkdev.info
  Support: https://nk.ticksy.com
  License: You must have a valid license purchased only from ThemeForest (the above link) in order to legally use the theme for your project.
  Copyright 2017.
-->

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Triton Communications Private Limited.</title>

    <meta name="description" content="Piroll - Clean & Minimal Portfolio HTML template.">
    <meta name="keywords" content="portfolio, clean, minimal, blog, template, portfolio website">
    <meta name="author" content="robirurk">

    <link rel="icon" type="image/png" href="assets/images/favicon.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500%7cNunito+Sans:400,600,700%7cPT+Serif:400,400i" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Stroke 7 -->
    <link rel="stylesheet" href="assets/bower_components/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="assets/bower_components/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/default-skin/default-skin.css">

    <!-- Piroll -->
    <link rel="stylesheet" href="assets/css/piroll.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>


</head>


<body>
<?php include 'layout/header.php';?>


<div class="nk-main">

        <!--<div class="nk-gap-6 mnt-4"></div>-->

        <div class="container-fluid">
            <div class="nk-portfolio-single nk-portfolio-single-half">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="nk-sidebar-sticky" data-offset-top="0">
                            <div class="nk-portfolio-info mlauto pl-40">
                                <h1 class="nk-portfolio-title">YES Bank</h1>
                                <h4 class="nk-portfolio-title">India bole YES!</h4>
                                <div class="nk-portfolio-text">
<!--                                    <p>We were given a clean slate and the first thing we wrote on it was the name. <b>YES.</b> Simple yet powerful, a 3-lettered word that means the world. A word that opens eyes and minds the minute it’s pronounced. </p>-->
<!--                                   <p>YES BANK was the start of something big. We focused on expertise that made the corporate banking customer, feel at ease. We fashioned disruptive ideas that made banking easier, efficient, and personal. Like India’s 1st ‘Green Direct Mailer’ that’s recycled every year, thanks to its overwhelming popularity. The mailer, a money plant epitomized growth and prosperity. When YES BANK forayed into the Retail space, we zeroed in on the insight that ‘all life-changing moments begin with a YES’. Built on ‘YES’, the campaign created a warm, friendly image for the bank. We united the willingness of ‘yes’ with the mental make-up of the country and what emerged was an exuberant and inspired ‘INDIA BOLE YES’ rallying cry that represented the New India. </p>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 bg-black">
                        <div class="nk-portfolio-images">
                            <div class="js-video">
                                <video poster="assets/case-study/6/1-poster.png" controls>
                                    <source src="assets/case-study/6/1.mp4" type="video/mp4">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <div class="js-video">
                                <video poster="assets/case-study/6/2-poster.png" controls>
                                    <source src="assets/case-study/6/2.mp4" type="video/mp4">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <div class="js-video">
                                <video poster="assets/case-study/6/3-poster.png" controls>
                                    <source src="assets/case-study/6/3.mp4" type="video/mp4">
                                    Your browser does not support HTML5 video.
                                </video>
                            </div>
                            <img src="assets/case-study/6/1.jpg" alt="case-study">
                            <img src="assets/case-study/6/2.jpg" alt="case-study">
                            <img src="assets/case-study/6/3.jpg" alt="case-study">
                            <img src="assets/case-study/6/4.jpg" alt="case-study">
                            <img src="assets/case-study/6/5.jpg" alt="case-study">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- START: Pagination -->
        <div class="nk-pagination nk-pagination-center">
            <div class="container">
                <a class="nk-pagination-prev" style="text-align: right;" href="work-5.php">
                    <span class="pe-7s-angle-left"></span> Previous Project</a>
                <a class="nk-pagination-center" href="work.php">
                    <span class="nk-icon-squares"></span>
                </a>
                <a class="nk-pagination-next" href="work-7.php">Next Project <span class="pe-7s-angle-right"></span> </a>
            </div>
        </div>
        <!-- END: Pagination -->



        <?php include 'layout/footer.php';?>

    </div>




    <!-- START: Scripts -->

    <!-- GSAP -->
    <script src="assets/bower_components/gsap/src/minified/TweenMax.min.js"></script>
    <script src="assets/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- nK Share -->
    <script src="assets/plugins/nk-share/nk-share.js"></script>

    <!-- Sticky Kit -->
    <script src="assets/bower_components/sticky-kit/dist/sticky-kit.min.js"></script>

    <!-- Jarallax -->
    <script src="assets/bower_components/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/bower_components/jarallax/dist/jarallax-video.min.js"></script>

    <!-- Flickity -->
    <script src="assets/bower_components/flickity/dist/flickity.pkgd.min.js"></script>

    <!-- Isotope -->
    <script src="assets/bower_components/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Photoswipe -->
    <script src="assets/bower_components/photoswipe/dist/photoswipe.min.js"></script>
    <script src="assets/bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script>

    <!-- Jquery Form -->
    <script src="assets/bower_components/jquery-form/dist/jquery.form.min.js"></script>

    <!-- Jquery Validation -->
    <script src="assets/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>

    <!-- Hammer.js -->
    <script src="assets/bower_components/hammer.js/hammer.min.js"></script>

    <!-- Social Likes -->
    <script src="assets/bower_components/social-likes/dist/social-likes.min.js"></script>

    <!-- NanoSroller -->
    <script src="assets/bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script>

    <!-- Keymaster -->
    <script src="assets/bower_components/keymaster/keymaster.js"></script>

    <!-- Piroll -->
    <script src="assets/js/piroll.min.js"></script>
    <script src="assets/js/piroll-init.js"></script>

    <!-- Demo -->
    <script src="assets/js/demo.js"></script>
    <!-- END: Scripts -->


</body>

</html>