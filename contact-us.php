<!DOCTYPE html>
<!--
  Name: Piroll - Minimal & Clean Portfolio HTML Template
  Version: 1.0.0
  Author: robirurk, nK
  Website: https://nkdev.info
  Purchase: https://nkdev.info
  Support: https://nk.ticksy.com
  License: You must have a valid license purchased only from ThemeForest (the above link) in order to legally use the theme for your project.
  Copyright 2017.
-->

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Triton Communications Private Limited.</title>

    <meta name="description" content="Piroll - Clean & Minimal Portfolio HTML template.">
    <meta name="keywords" content="portfolio, clean, minimal, blog, template, portfolio website">
    <meta name="author" content="robirurk">

    <link rel="icon" type="image/png" href="assets/images/favicon.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500%7cNunito+Sans:400,600,700%7cPT+Serif:400,400i" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Stroke 7 -->
    <link rel="stylesheet" href="assets/bower_components/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="assets/bower_components/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/default-skin/default-skin.css">

    <!-- Piroll -->
    <link rel="stylesheet" href="assets/css/piroll.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>



</head>


<body>
    <?php include 'layout/header.php';?>


<div class="nk-main bg-white">



        <!-- START: Contact Info -->
        <div class="container">
<!--            <div class="nk-gap-1"></div>-->

            <div class="col-lg-6">
                <!-- START: Tabs -->
                <div class="nk-tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active mumbai" href="#tabs-1-1" role="tab" data-toggle="tab">Mumbai</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link ahmedabad" href="#tabs-1-2" role="tab" data-toggle="tab">Ahmedabad</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link bengaluru" href="#tabs-1-3" role="tab" data-toggle="tab">Bengaluru</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link delhi" href="#tabs-1-4" role="tab" data-toggle="tab">Delhi</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1">
                            <ul class="nk-contact-info">
                                <li>
                                    <strong>Address:</strong> Moderna House, 88/C, Mezzanine Floor, Old Prabhadevi Marg, Prabhadevi, Mumbai, Maharashtra 400025</li>
                                <li>
                                    <strong>Phone:</strong> +91 22 4093 0500</li>
                                <li>
<!--                                    <strong>Email:</strong> amrita.shetty@tritoncom.com</li>-->
                                    <strong>Email:</strong> <a href="mailto:virendra.saini@tritoncom.com">virendra.saini@tritoncom.com</a></li>
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-2">
                            <ul class="nk-contact-info">
                                <li>
                                    <strong>Address:</strong> B907/908, Premium House, Opp. Gandhigram Station, Off Ashram House, Ahmedabad 380 009</li>
                                <li>
                                    <strong>Phone:</strong> 079 4022 6551/52, 079 2658 6551/52</li>
                                <li>
                                    <strong>Email:</strong> <a href="mailto:mayur.rana@tritoncom.com">mayur.rana@tritoncom.com</a></li>
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-3">
                            <ul class="nk-contact-info">
                                <li>
                                    <strong>Address:</strong> #32, 3rd Floor, 2nd Block, Near Anglo-Indian Colony, Austin Town,Bengaluru 580 047</li>
                                <li>
                                    <strong>Phone:</strong> 080 4242 0900/918</li>
                                <li>
                                    <strong>Email:</strong> <a href="mailto:ganesh.kamath@tritoncom.com">ganesh.kamath@tritoncom.com</a></li>
                            </ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tabs-1-4">
                            <ul class="nk-contact-info">
                                <li>
                                    <strong>Address:</strong> Triton Communications Private Limited, Upper Ground Floor, Plot no. 15, Sector 44, Gurugram-122003</li>
                                <li>
                                    <strong>Phone:</strong> +91124-4665050</li>
                                <li>
                                    <strong>Email:</strong> <a href="mailto:naveen.saraswat@tritoncom.com">naveen.saraswat@tritoncom.com</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- END: Tabs -->
            </div>

            <div class="nk-gap-1"></div>
        </div>
        <!-- END: Contact Info -->




        <!-- START: Google Map -->
        <div id="mumbai-map-contact" class="nk-gmaps nk-gmaps-lg"></div>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCoSHvWy9b6PJB-BwBhuFmXYfUqF6JfcpQ&sensor=false"></script>
        <script>
            function initializeGmaps() {
                var LatLng = {
                    lat: 19.0148617,
                    lng: 72.8223487
                };
                var mapCanvas = document.getElementById('mumbai-map-contact');
                var mapOptions = {
                    center: LatLng,
                    scrollwheel: false,
                    zoom: 14,
                    backgroundColor: 'none',
//                    mapTypeId: 'nKStyle'
                };
                var map = new google.maps.Map(mapCanvas, mapOptions);
                var marker = new google.maps.Marker({
                    position: LatLng,
                    map: map,
                    icon: 'assets/images/marker.png',
                    title: 'Mumbai'
                });

                // style from https://snazzymaps.com/style/151/ultra-light-with-labels
                var styledMapType = new google.maps.StyledMapType([{
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e9e9e9"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 17
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 29
                    }, {
                        "weight": 0.2
                    }]
                }, {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 18
                    }]
                }, {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#dedede"
                    }, {
                        "lightness": 21
                    }]
                }, {
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "visibility": "on"
                    }, {
                        "color": "#ffffff"
                    }, {
                        "lightness": 16
                    }]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "saturation": 36
                    }, {
                        "color": "#333333"
                    }, {
                        "lightness": 40
                    }]
                }, {
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "transit",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f2f2f2"
                    }, {
                        "lightness": 19
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 20
                    }]
                }, {
                    "featureType": "administrative",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#fefefe"
                    }, {
                        "lightness": 17
                    }, {
                        "weight": 1.2
                    }]
                }], {
                    name: 'nKStyle'
                });
                map.mapTypes.set('nKStyle', styledMapType);
            }
            if (typeof google !== 'undefined') {
                google.maps.event.addDomListener(window, 'load', initializeGmaps);
            }
        </script>
        <!-- END: Google Map -->


    <!-- START: Google Map -->
    <div id="ahmedabad-map-contact" class="nk-gmaps nk-gmaps-lg" style="display: none"></div>
    <script>
        function initializeGmaps() {
            var LatLng = {
                lat: 23.0253068,
                lng: 72.5692167
            };
            var mapCanvas = document.getElementById('ahmedabad-map-contact');
            var mapOptions = {
                center: LatLng,
                scrollwheel: false,
                zoom: 14,
                backgroundColor: 'none',
//                    mapTypeId: 'nKStyle'
            };
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                icon: 'assets/images/marker.png',
                title: 'Ahmedabad'
            });

            // style from https://snazzymaps.com/style/151/ultra-light-with-labels
            var styledMapType = new google.maps.StyledMapType([{
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e9e9e9"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 29
                }, {
                    "weight": 0.2
                }]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 18
                }]
            }, {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 21
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#dedede"
                }, {
                    "lightness": 21
                }]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "saturation": 36
                }, {
                    "color": "#333333"
                }, {
                    "lightness": 40
                }]
            }, {
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f2f2f2"
                }, {
                    "lightness": 19
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 17
                }, {
                    "weight": 1.2
                }]
            }], {
                name: 'nKStyle'
            });
            map.mapTypes.set('nKStyle', styledMapType);
        }
        if (typeof google !== 'undefined') {
            google.maps.event.addDomListener(window, 'load', initializeGmaps);
        }
    </script>
    <!-- END: Google Map -->


    <!-- START: Google Map -->
    <div id="bengaluru-map-contact" class="nk-gmaps nk-gmaps-lg" style="display: none"></div>
    <script>
        function initializeGmaps() {
            var LatLng = {
                lat: 19.0148617,
                lng: 72.8223487
            };
            var mapCanvas = document.getElementById('bengaluru-map-contact');
            var mapOptions = {
                center: LatLng,
                scrollwheel: false,
                zoom: 14,
                backgroundColor: 'none',
//                    mapTypeId: 'nKStyle'
            };
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                icon: 'assets/images/marker.png',
                title: 'Bengaluru'
            });

            // style from https://snazzymaps.com/style/151/ultra-light-with-labels
            var styledMapType = new google.maps.StyledMapType([{
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e9e9e9"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 29
                }, {
                    "weight": 0.2
                }]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 18
                }]
            }, {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 21
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#dedede"
                }, {
                    "lightness": 21
                }]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "saturation": 36
                }, {
                    "color": "#333333"
                }, {
                    "lightness": 40
                }]
            }, {
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f2f2f2"
                }, {
                    "lightness": 19
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 17
                }, {
                    "weight": 1.2
                }]
            }], {
                name: 'nKStyle'
            });
            map.mapTypes.set('nKStyle', styledMapType);
        }
        if (typeof google !== 'undefined') {
            google.maps.event.addDomListener(window, 'load', initializeGmaps);
        }
    </script>
    <!-- END: Google Map -->


    <!-- START: Google Map -->
    <div id="delhi-map-contact" class="nk-gmaps nk-gmaps-lg" style="display: none"></div>
    <script>
        function initializeGmaps() {
            var LatLng = {
                lat: 28.4493124,
                lng: 77.0715442
            };
            var mapCanvas = document.getElementById('delhi-map-contact');
            var mapOptions = {
                center: LatLng,
                scrollwheel: false,
                zoom: 14,
                backgroundColor: 'none',
//                    mapTypeId: 'nKStyle'
            };
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                icon: 'assets/images/marker.png',
                title: 'Delhi'
            });

            // style from https://snazzymaps.com/style/151/ultra-light-with-labels
            var styledMapType = new google.maps.StyledMapType([{
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e9e9e9"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 29
                }, {
                    "weight": 0.2
                }]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 18
                }]
            }, {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 21
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#dedede"
                }, {
                    "lightness": 21
                }]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "saturation": 36
                }, {
                    "color": "#333333"
                }, {
                    "lightness": 40
                }]
            }, {
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f2f2f2"
                }, {
                    "lightness": 19
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 17
                }, {
                    "weight": 1.2
                }]
            }], {
                name: 'nKStyle'
            });
            map.mapTypes.set('nKStyle', styledMapType);
        }
        if (typeof google !== 'undefined') {
            google.maps.event.addDomListener(window, 'load', initializeGmaps);
        }
    </script>
    <!-- END: Google Map -->







        <?php include 'layout/footer.php';?>


    </div>




    <!-- START: Scripts -->

    <!-- GSAP -->
    <script src="assets/bower_components/gsap/src/minified/TweenMax.min.js"></script>
    <script src="assets/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- nK Share -->
    <script src="assets/plugins/nk-share/nk-share.js"></script>

    <!-- Sticky Kit -->
    <script src="assets/bower_components/sticky-kit/dist/sticky-kit.min.js"></script>

    <!-- Jarallax -->
    <script src="assets/bower_components/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/bower_components/jarallax/dist/jarallax-video.min.js"></script>

    <!-- Flickity -->
    <script src="assets/bower_components/flickity/dist/flickity.pkgd.min.js"></script>

    <!-- Isotope -->
    <script src="assets/bower_components/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Photoswipe -->
    <script src="assets/bower_components/photoswipe/dist/photoswipe.min.js"></script>
    <script src="assets/bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script>

    <!-- Jquery Form -->
    <script src="assets/bower_components/jquery-form/dist/jquery.form.min.js"></script>

    <!-- Jquery Validation -->
    <script src="assets/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>

    <!-- Hammer.js -->
    <script src="assets/bower_components/hammer.js/hammer.min.js"></script>

    <!-- Social Likes -->
    <script src="assets/bower_components/social-likes/dist/social-likes.min.js"></script>

    <!-- NanoSroller -->
    <script src="assets/bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script>

    <!-- Keymaster -->
    <script src="assets/bower_components/keymaster/keymaster.js"></script>

    <!-- Piroll -->
    <script src="assets/js/piroll.min.js"></script>
    <script src="assets/js/piroll-init.js"></script>

    <!-- Demo -->
    <script src="assets/js/demo.js"></script>
    <!-- END: Scripts -->

<script>
    $(".mumbai").on("click",function(){
        $("#mumbai-map-contact").css("display","block")
        $("#ahmedabad-map-contact").css("display","none")
        $("#bengaluru-map-contact").css("display","none")
        $("#delhi-map-contact").css("display","none")
    });
    $(".ahmedabad").on("click",function(){
        $("#mumbai-map-contact").css("display","none")
        $("#ahmedabad-map-contact").css("display","block")
        $("#bengaluru-map-contact").css("display","none")
        $("#delhi-map-contact").css("display","none")
    });
    $(".bengaluru").on("click",function(){
        $("#mumbai-map-contact").css("display","none")
        $("#ahmedabad-map-contact").css("display","none")
        $("#bengaluru-map-contact").css("display","block")
        $("#delhi-map-contact").css("display","none")
    });
    $(".delhi").on("click",function(){
        $("#mumbai-map-contact").css("display","none")
        $("#ahmedabad-map-contact").css("display","none")
        $("#bengaluru-map-contact").css("display","none")
        $("#delhi-map-contact").css("display","block")
    });
</script>
</body>

</html>