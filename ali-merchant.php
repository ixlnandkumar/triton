<!DOCTYPE html>
<!--
  Name: Piroll - Minimal & Clean Portfolio HTML Template
  Version: 1.0.0
  Author: robirurk, nK
  Website: https://nkdev.info
  Purchase: https://nkdev.info
  Support: https://nk.ticksy.com
  License: You must have a valid license purchased only from ThemeForest (the above link) in order to legally use the theme for your project.
  Copyright 2017.
-->

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Triton Communications Private Limited.</title>

    <meta name="description" content="Piroll - Clean & Minimal Portfolio HTML template.">
    <meta name="keywords" content="portfolio, clean, minimal, blog, template, portfolio website">
    <meta name="author" content="robirurk">

    <link rel="icon" type="image/png" href="assets/images/favicon.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- START: Styles -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500%7cNunito+Sans:400,600,700%7cPT+Serif:400,400i" rel="stylesheet" type="text/css">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="assets/bower_components/fontawesome/css/font-awesome.min.css">

    <!-- Stroke 7 -->
    <link rel="stylesheet" href="assets/bower_components/pixeden-stroke-7-icon/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">

    <!-- Flickity -->
    <link rel="stylesheet" href="assets/bower_components/flickity/dist/flickity.min.css">

    <!-- Photoswipe -->
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="assets/bower_components/photoswipe/dist/default-skin/default-skin.css">

    <!-- Piroll -->
    <link rel="stylesheet" href="assets/css/piroll.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="assets/css/custom.css">

    <!-- END: Styles -->

    <!-- jQuery -->
    <script src="assets/bower_components/jquery/dist/jquery.min.js"></script>


</head>


<body>
    <?php include 'layout/header.php';?>


<div class="nk-main">

        <!--<div class="nk-gap-6 mnt-4"></div>-->

        <div class="container-fluid">
            <div class="nk-portfolio-single nk-portfolio-single-half">
                <div class="row">
                    <div class="col-lg-6 push-lg-6">
                        <div class="nk-sidebar-sticky" data-offset-top="0">
                            <div class="nk-portfolio-info">
                                <h1 class="nk-portfolio-title">Ali Merchant</h1>
                                <h4 class="nk-portfolio-title" style="color: #666666">Director </h4>
                                <div class="nk-portfolio-text">
                                   <p>An attorney-at-law, Ali brought his fiercely combative skills to Advertising. The rest, as they say, is history. In 35 years, he has fought the cases of small and big brands; national and international clients like Procter & Gamble, Johnson & Johnson, Philips, National (Japan), Brooke Bond, Eureka Forbes, Adani, YES BANK, Force Motors, Wagh Bakri and Karnataka Tourism, to name a few.</p>
                                   <p>Prior to co-founding Triton with Munawar Syed, he was Deputy Managing Director of Everest Advertising Pvt. Ltd. Ali earned his stripes at Bozell & Jacobs (USA) and Saatchi & Saatchi (UK).</p>
                                   <p>He is actively involved with the International Advertising Association and the Rotary Club of Bombay, besides being a Member of the Managing Committee for the Council of Fair Business Practices.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 pull-lg-6">
                        <div class="nk-portfolio-images">
                            <img src="assets/images/people/ali_m.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- START: Pagination -->
        <div class="nk-pagination nk-pagination-center">
            <div class="container">
                <span class="nk-pagination-prev"></span>
                <a class="nk-pagination-center" href="people.php">
                    <span class="nk-icon-squares"></span>
                </a>
                <a class="nk-pagination-next" href="munawar-syed.php">Next <span class="pe-7s-angle-right"></span> </a>
            </div>
        </div>
        <!-- END: Pagination -->




        <?php include 'layout/footer.php';?>

    </div>




    <!-- START: Scripts -->

    <!-- GSAP -->
    <script src="assets/bower_components/gsap/src/minified/TweenMax.min.js"></script>
    <script src="assets/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>

    <!-- Bootstrap -->
    <script src="assets/bower_components/tether/dist/js/tether.min.js"></script>
    <script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- nK Share -->
    <script src="assets/plugins/nk-share/nk-share.js"></script>

    <!-- Sticky Kit -->
    <script src="assets/bower_components/sticky-kit/dist/sticky-kit.min.js"></script>

    <!-- Jarallax -->
    <script src="assets/bower_components/jarallax/dist/jarallax.min.js"></script>
    <script src="assets/bower_components/jarallax/dist/jarallax-video.min.js"></script>

    <!-- Flickity -->
    <script src="assets/bower_components/flickity/dist/flickity.pkgd.min.js"></script>

    <!-- Isotope -->
    <script src="assets/bower_components/isotope/dist/isotope.pkgd.min.js"></script>

    <!-- Photoswipe -->
    <script src="assets/bower_components/photoswipe/dist/photoswipe.min.js"></script>
    <script src="assets/bower_components/photoswipe/dist/photoswipe-ui-default.min.js"></script>

    <!-- Jquery Form -->
    <script src="assets/bower_components/jquery-form/dist/jquery.form.min.js"></script>

    <!-- Jquery Validation -->
    <script src="assets/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>

    <!-- Hammer.js -->
    <script src="assets/bower_components/hammer.js/hammer.min.js"></script>

    <!-- Social Likes -->
    <script src="assets/bower_components/social-likes/dist/social-likes.min.js"></script>

    <!-- NanoSroller -->
    <script src="assets/bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js"></script>

    <!-- Keymaster -->
    <script src="assets/bower_components/keymaster/keymaster.js"></script>

    <!-- Piroll -->
    <script src="assets/js/piroll.min.js"></script>
    <script src="assets/js/piroll-init.js"></script>

    <!-- Demo -->
    <script src="assets/js/demo.js"></script>
    <!-- END: Scripts -->


</body>

</html>